package no.uib.eol071.sentiment.weighted;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Weighter {
	int i = 0;
	ArrayList<String> posKeyList = new ArrayList<String>();
	ArrayList<String> negKeyList = new ArrayList<String>();
	ArrayList<Integer> weights = new ArrayList<Integer>();
	ArrayList<String> documents = new ArrayList<String>();
	HashMap<String, Integer> weightMap = new HashMap<String, Integer>();
	HashMap<String, Integer> documentMap = new HashMap<String, Integer>();
	HashMap<String, Integer> stopMap = new HashMap<String, Integer>();



	public Weighter(){
		stopWords();
		run();
		map();
	}

	public void stopWords(){
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader("stopwords.txt"));
			String stopLine = br.readLine();
			while (stopLine != null) {
				stopMap.put(stopLine, 1);
				stopLine = br.readLine();
			}
			br.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String stemTerm (String term) {
		PorterStemmer stemmer = new PorterStemmer();
		return stemmer.stem(term);
	}


	private void map() {
		System.out.println(weightMap.size());

		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("test.txt", false));
			double weight;
			double idf;
			for(int i = 0; i < posKeyList.size(); i++){
				String key = posKeyList.get(i);
				bw.append(key.toLowerCase());
				bw.append(",");
				weight = weightMap.get(key);
				idf = documentMap.get(key);
				idf = 25000/idf;
				idf = Math.log10(idf);
				if(weight < 0){
					weight = Math.log10(Math.abs(weight));
					if(idf > 0){
						weight = weight*idf;
					}
					weight = -weight;
				}else{
					weight = Math.log10(Math.abs(weight));
					if(idf > 0){
						weight = weight*idf;
					}
				}
				if(Double.isInfinite(weight)){
					weight = 0;
				}
				bw.append(weight + "");
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private boolean match(String term){
		Pattern p = Pattern.compile("^[a-zA-Z]+$");
		Matcher m = p.matcher(term);
		boolean found = m.find();
		return found;
	}

	public void run() {

		//for all positive texts in collection go through and add up words
		File dir = new File("pos");
		File[] directoryListing = dir.listFiles();
		if (directoryListing != null) {
			for (File child : directoryListing) {
				add(child);
			}
			System.out.println("pos done");
		} else {
		}
		//for all negative texts in collection go through and add up words
		File dir2 = new File("neg");
		File[] directoryListing2 = dir2.listFiles();
		if (directoryListing2 != null) {
			for (File child : directoryListing2) {
				subtract(child);
			}
			System.out.println("neg done");
		} else {
		}

	}

	private void add(File child){
		try {
			BufferedReader br = new BufferedReader(new FileReader(child));
			String positiveLine = br.readLine();
			while (positiveLine != null) {
				HashMap<String, Integer> tempMap = new HashMap<String, Integer>();
				for(String word : positiveLine.split(" ")) {
					if(match(word)){
						word = stemTerm(word);
						if(!stopMap.containsKey(word.toLowerCase())){

							if(!tempMap.containsKey(word)){
								tempMap.put(word, 1);
								int docTemp;
								if(documentMap.get(word.toLowerCase()) == null){
									docTemp = 0;
								}else{
									docTemp = documentMap.get(word.toLowerCase());
								}
								if(docTemp < 1){
									documentMap.put(word.toLowerCase(), 1);						
								}
								else{
									docTemp ++;
									documentMap.put(word.toLowerCase(), docTemp);
								}
							}
							int temp;
							if(weightMap.get(word.toLowerCase()) == null){
								temp = 0;
							}else{
								temp = weightMap.get(word.toLowerCase());
							}
							if(temp < 1){
								weightMap.put(word.toLowerCase(), 1);						
								posKeyList.add(word.toLowerCase());
							}
							else{
								temp ++;
								weightMap.put(word.toLowerCase(), temp);
							}
							positiveLine = br.readLine();
						}
					}
				}
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void subtract (File child){
		try {
			BufferedReader br = new BufferedReader(new FileReader(child));
			String negativeLine = br.readLine();
			while (negativeLine != null) {
				HashMap<String, Integer> tempMap = new HashMap<String, Integer>();
				for(String word : negativeLine.split(" ")) {
					if(match(word)){
						word = stemTerm(word);
						if(!stopMap.containsKey(word.toLowerCase())){
							if(!tempMap.containsKey(word)){
								tempMap.put(word, 1);
								int docTemp;
								if(documentMap.get(word.toLowerCase()) == null){
									docTemp = 0;
								}else{
									docTemp = documentMap.get(word.toLowerCase());
								}
								if(docTemp < 1){
									documentMap.put(word.toLowerCase(), 1);						
								}
								else{
									docTemp ++;
									documentMap.put(word.toLowerCase(), docTemp);
								}
							}
							boolean newWord = false;
							int temp;
							if(weightMap.get(word.toLowerCase()) == null){
								temp = 0;
								newWord = true;
							}else{
								temp = weightMap.get(word.toLowerCase());
							}
							if(newWord){
								weightMap.put(word.toLowerCase(), 1);						
								posKeyList.add(word.toLowerCase());
							}
							else{
								temp --;
								weightMap.put(word.toLowerCase(), temp);
							}
							negativeLine = br.readLine();
						}
					}
				}
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
