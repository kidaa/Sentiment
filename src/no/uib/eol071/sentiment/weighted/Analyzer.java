package no.uib.eol071.sentiment.weighted;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class Analyzer {
	String file;
	double total = 0.0;
    HashMap<String, Double> weightMap;
	
	public Analyzer(String string, HashMap weightMap) {
		this.file = string;
        this.weightMap = weightMap;
//        hash();
		analyze();
	}

    public Analyzer(String sentence, boolean b, HashMap weightMap) {
        for(String word : sentence.split(" ")) {
            this.weightMap = weightMap;
            word = stemTerm(word);
            compare(word.toLowerCase());
        }
    }

    public String stemTerm (String term) {
		PorterStemmer stemmer = new PorterStemmer();
		return stemmer.stem(term);
	}

	public void analyze(){
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));
			String line = br.readLine();
			while(line != null){
				for(String word : line.split(" ")) {
					word = stemTerm(word);
					compare(word.toLowerCase());
				}
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void compare(String word) {
		if(weightMap.containsKey(word)){
            total += total + weightMap.get(word);
        }

	}
	public double getTotal() {
		return total;
	}
}
