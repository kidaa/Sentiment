package no.uib.eol071.sentiment.cooccurrence;

import no.uib.eol071.sentiment.weighted.PorterStemmer;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Erik on 18.10.2014.
 */
public class CoOccurrenceAnalyzer {
    String file;
    double total = 0.0;
    HashMap<String, Double> weightMap;

    public CoOccurrenceAnalyzer(String string, HashMap weightMap) {
        this.file = string;
        this.weightMap = weightMap;
//        hash();
        analyze();
    }

    public CoOccurrenceAnalyzer(String sentence, boolean b, HashMap weightMap) {
        for(String word : sentence.split(" ")) {
            this.weightMap = weightMap;
            word = stemTerm(word);
            compare(word.toLowerCase());
        }
    }

    private boolean match(String term){
        Pattern p = Pattern.compile("^[a-zA-Z]+$");
        Matcher m = p.matcher(term);
        boolean found = m.find();
        return found;
    }

    private ArrayList<String> coOccurenceArray(String line) {
        String[] temp = line.split("[!?\\.]");
        ArrayList<String> list = new ArrayList<String>();
        for (String child : temp) {
            String[] parts = child.split(" ");
            for (int i = 0; i < parts.length - 1; i++) {
                String one = stemTerm(parts[i].toLowerCase()
                );
                String two = stemTerm(parts[i + 1].toLowerCase()
                );
                while (!match(one) && !one.equals("")) {
                    int length = one.length();
                    one = one.substring(0, length - 1);
                }
                while (!match(two) && !two.equals("")) {
                    int length = two.length();
                    two = two.substring(0, length - 1);
                }
                if (!one.equals("")) {
                    list.add(one + " " + two);
                }
            }
        }
        return list;
    }

    public String stemTerm (String term) {
        PorterStemmer stemmer = new PorterStemmer();
        return stemmer.stem(term);
    }

    public void analyze(){
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = br.readLine();
            while(line != null){
                for(String word : line.split(" ")) {
                    word = stemTerm(word);
                    compare(word.toLowerCase());
                }
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    private void compare(String word) {
        if(weightMap.containsKey(word)){
            total += total + weightMap.get(word);
        }

    }
    public double getTotal() {
        return total;
    }

}
