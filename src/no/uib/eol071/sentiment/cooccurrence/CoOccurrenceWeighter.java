package no.uib.eol071.sentiment.cooccurrence;

import no.uib.eol071.sentiment.weighted.PorterStemmer;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Erik on 09.10.2014.
 */
public class CoOccurrenceWeighter {
    HashMap<String, Integer> stopMap = new HashMap<String, Integer>();
    HashMap<String, Integer> weightMap = new HashMap<String, Integer>();
    HashMap<String, Integer> documentMap = new HashMap<String, Integer>();
    ArrayList<String> key = new ArrayList<String>();

    public CoOccurrenceWeighter() {
        run();
        map();
    }

    private void map() {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter("co.txt", false));
            for(int i = 0; i < key.size(); i++){
                String write = key.get(i) +","+ weightMap.get(key.get(i));
                bw.append(write);
                bw.newLine();
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String stemTerm (String term) {
        PorterStemmer stemmer = new PorterStemmer();
        return stemmer.stem(term);
    }

    private boolean match(String term){
        Pattern p = Pattern.compile("^[a-zA-Z]+$");
        Matcher m = p.matcher(term);
        boolean found = m.find();
        return found;
    }

    public void run() {

        //for all positive texts in collection go through and add up words
        File dir = new File("pos");
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                add(child);
            }
            System.out.println("pos done");
        } else {
        }
        //for all negative texts in collection go through and add up words
        File dir2 = new File("neg");
        File[] directoryListing2 = dir2.listFiles();
        if (directoryListing2 != null) {
            for (File child : directoryListing2) {
                subtract(child);
            }
            System.out.println("neg done");
        } else {
        }

    }

    private ArrayList<String> coOccurenceArray(String line){
        String[] temp = line.split("[!?\\.]");
        ArrayList<String> list = new ArrayList<String>();
        for(String child:temp){
            String[] parts = child.split(" ");
            for(int i = 0; i<parts.length-1; i++){
                String one = stemTerm(parts[i].toLowerCase()
                );
                String two = stemTerm(parts[i+1].toLowerCase()
                );
                while(!match(one) && !one.equals("")){
                    int length = one.length();
                    one = one.substring(0, length-1);
                }
                while(!match(two) && !two.equals("")){
                    int length = two.length();
                    two = two.substring(0, length-1);
                }
                if(!one.equals("")) {
                    list.add(one + " " + two);
                }
            }
        }
        return list;
    }

    private void add(File child){
        try {
            BufferedReader br = new BufferedReader(new FileReader(child));
            String positiveLine = br.readLine();
            String fullText = "";
            while (positiveLine != null) {
                fullText = fullText.concat(positiveLine);
                positiveLine = br.readLine();
            }
            ArrayList<String> temp = coOccurenceArray(fullText);
            Boolean newDoc = true;
            for(int i = 0; i < temp.size(); i++){
                int weight = 1;
                String co = temp.get(i);
                if(weightMap.containsKey(co)){
                    weight = weightMap.get(co);
                    weight++;
                    weightMap.put(co,weight);
                }else{
                    weightMap.put(co,weight);
                    key.add(co);
                }
            }

            br.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void subtract (File child){
        try {
            BufferedReader br = new BufferedReader(new FileReader(child));
            String negativeLine = br.readLine();
            String fullText = "";
            while (negativeLine != null) {
                fullText = fullText.concat(negativeLine);
                negativeLine = br.readLine();
            }
            ArrayList<String> temp = coOccurenceArray(fullText);
            Boolean newDoc = true;
            for(int i = 0; i < temp.size(); i++){
                int weight = -1;
                String co = temp.get(i);
                if(weightMap.containsKey(co)){
                    weight = weightMap.get(co);
                    weight--;
                    weightMap.put(co,weight);
                }else{
                    weightMap.put(co,weight);
                    key.add(co);
                }
            }
            br.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
