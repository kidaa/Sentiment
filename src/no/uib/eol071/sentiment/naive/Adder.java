package no.uib.eol071.sentiment.naive;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;


public class Adder {
	double neg = 0;
	double pos = 0;
	String text = "";

	public Adder(String text){
		this.text = text;
		read(text);
	}


	public void read(String text) {

		try {
			BufferedReader negativeReader = new BufferedReader(new FileReader("negative.txt"));
			BufferedReader positiveReader = new BufferedReader(new FileReader("positive.txt"));
			String negativeLine = negativeReader.readLine();
			String positiveLine = positiveReader.readLine();
			String temp = "";
			BufferedReader blogReader = new BufferedReader(new FileReader(text));
			String blogLine = blogReader.readLine();
			while (blogLine != null) {
				temp = temp.concat(blogLine + " ");
				blogLine = blogReader.readLine();
			}
			StringTokenizer st = new StringTokenizer(temp);
			while (st.hasMoreTokens()) {
				String t = st.nextToken();
				while (negativeLine != null) {
					if(t.equalsIgnoreCase(negativeLine)){
						neg++;
					}
					negativeLine = negativeReader.readLine();
				}
				negativeReader = new BufferedReader(new FileReader("negative.txt"));
				negativeLine = negativeReader.readLine();
			}
			negativeReader.close();

			st = new StringTokenizer(temp);
			while (st.hasMoreTokens()) {
				String t = st.nextToken();
				while (positiveLine != null) {
					if(t.equalsIgnoreCase(positiveLine)){
						pos++;
					}
					positiveLine = positiveReader.readLine();
				}
				positiveReader = new BufferedReader(new FileReader("positive.txt"));
				positiveLine = positiveReader.readLine();
			}
			positiveReader.close();
			blogReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	public void present() {
		double tot = neg + pos;
		neg = neg/tot*100;
		pos = pos/tot*100;
	}

    public boolean posneg(){
        if(neg>pos){
            return false;
        }else{
            return true;
        }
    }
	public double getNeg() {
		return neg;
	}

	public double getPos() {
		return pos;
	}
}
