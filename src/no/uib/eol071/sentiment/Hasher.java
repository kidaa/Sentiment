package no.uib.eol071.sentiment;

import java.io.*;
import java.util.HashMap;

/**
 * Created by Erik on 18.10.2014.
 */
public class Hasher {
    HashMap<String, Double> weightMap = new HashMap<String, Double>();
    public Hasher() {
        hash();
    }

    public void hash(){
        try {
            BufferedReader br = new BufferedReader(new FileReader("test.txt"));
            String line = br.readLine();
            while(line != null){
                String[] temp = line.split(",");
                weightMap.put(temp[0],Double.parseDouble(temp[1]));
                line = br.readLine();
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public HashMap<String, Double> getWeightMap() {
        return weightMap;
    }
}
