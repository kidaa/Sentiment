package no.uib.eol071.sentiment.sentence;
import no.uib.eol071.sentiment.weighted.Analyzer;

import java.util.HashMap;

/**
 * Created by Erik on 05.10.2014.
 */
public class Sentence {

    boolean sentiment = false;
    String sentence;
    HashMap<String, Double> weightMap;

    public boolean getSentiment() {
        return sentiment;
    }

    public Sentence(String sentence, HashMap weightMap)
    {
        this.sentence = sentence;
        this.weightMap = weightMap;
    }

    public boolean Analyze(){
        Analyzer analyze = new Analyzer(sentence, false, weightMap);
        if (analyze.getTotal() >= 0){
            sentiment = true;
        }
        return sentiment;
    }

}
