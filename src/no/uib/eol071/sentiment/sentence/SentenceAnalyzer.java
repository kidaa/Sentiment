package no.uib.eol071.sentiment.sentence;

import java.io.*;
import java.util.HashMap;

/**
 * Created by Erik on 05.10.2014.
 */
public class SentenceAnalyzer {
File file;
boolean sentiment = false;
int posneg = 0;
    HashMap<String, Double> weightMap;

    public SentenceAnalyzer(File file, HashMap weightMap) {
        this.file = file;
        this.weightMap = weightMap;
        read();
    }

    public void read(){
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = br.readLine();
            while(line != null){
                String reg = "(?!)\\p{Punct}";
                String[] sentences = line.split("[!?\\.]");
                for (String sent : sentences) {
                    Sentence sentence = new Sentence(sent, weightMap);
                    if(sentence.Analyze()){
                        posneg ++;
                    }
                    else{
                        posneg--;
                    }
                }

                line = br.readLine();
            }
            br.close();
            if(posneg >= 0){
                sentiment = true;
            }else{
                sentiment = false;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public boolean getSentiment() {
        return sentiment;
    }
}
