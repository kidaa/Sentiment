package no.uib.eol071.sentiment;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import no.uib.eol071.sentiment.cooccurrence.CoOccurrenceWeighter;
import no.uib.eol071.sentiment.weighted.Analyzer;
import no.uib.eol071.sentiment.sentence.SentenceAnalyzer;


public class Main {

	public static void main(String[] args) {
        HashMap<String, Double> weightMap;
		double good = 0;
		double bad = 0;
		double total = 0;
//		Weighter weight = new Weighter();
		File dir = new File("pos");
        Hasher hasher = new Hasher();
        weightMap = hasher.getWeightMap();
        CoOccurrenceWeighter weighter = new CoOccurrenceWeighter();
		File[] directoryListing = dir.listFiles();
        System.out.println("start");
        long time = System.currentTimeMillis();
		if (directoryListing != null) {
			for (File child : directoryListing) {
				SentenceAnalyzer analyze = new SentenceAnalyzer(child, weightMap);
				if(analyze.getSentiment()){
					good++;
					total++;
				}else{
					bad++;
					total++;
				}
			}
//            for (File child : directoryListing) {
//                Analyzer analyze = new Analyzer(child.toString(), weightMap);
//                if(analyze.getTotal() >=0){
//                    good++;
//                    total++;
//                }else{
//                    bad++;
//                    total++;
//                }
//                System.out.println("Total = " + total + ". Good/Bad = " + good/total*100 + "%");
//            }
		} else {
		}
        System.out.println("Total = " + total + ". Good/Bad = " + good/total*100 + "%");
        long newTime = System.currentTimeMillis();
        long finished = newTime - time;
        double display = finished/1000;
        System.out.println(display + " seconds");
	}


}
